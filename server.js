// eslint-disable-next-line @typescript-eslint/no-var-requires
const jsonServer = require( 'json-server' );
const server = jsonServer.create();
const middlewares = jsonServer.defaults();

// Set default middlewares (logger, static, cors and no-cache)
server.use( middlewares );

// Add custom routes before JSON Server router
server.get( '/assesment', ( req, res ) => {
    const { page, limit, showError } = req.query || {};
    const pageNumber = Number( page );
    const limitNumber = Number( limit );
    if ( showError ) return res.send( 400 );
    const currentPageIndex = pageNumber * limitNumber;
    const list = [];
    for ( let i = currentPageIndex; i < currentPageIndex + limitNumber; i++ ) {
        list.push( {
            entityId: i,
            companyName: `ABC${i}`,
            location: `City State, Country ${i}`,
            latestKca: `BBB ${i}`,
            latestCe: 'AAA',
            alternateNames: [ 'ABC company', 'ABCD company' ],
            industry: [ `Industry ${i}`, `IndustryB ${i}` ],
        } );
    }
    const response = {
        total: 1000,
        list,
    };

    setTimeout((() => {
        res.jsonp( response );
    }), 3000)
    
    // res.send( 400 );
} );

// To handle POST, PUT and PATCH you need to use a body-parser
// You can use the one used by JSON Server
server.use( jsonServer.bodyParser );
server.use( ( req, res, next ) => {
    if ( req.method === 'POST' ) {
        req.body.createdAt = Date.now();
    }
    // Continue to JSON Server router
    next();
} );

server.listen( process.env.PORT||3002, () => {
    console.log( 'JSON Server is running in port : 3002' );
} );
